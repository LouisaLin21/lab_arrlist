import java.util.*;
public class ListPratice{
    public static int countUpperCase(String[] strings){
        //This class should count how many string in the array
        //contains only uppcase letters
        int uppcaseCount=0;
        for(i=0;i<strings.length;i++){
           //helper method needed
           if(isUpperCase(strings[i])==true){
               uppcaseCount++;
           }
        }
    }
    public static String[] getUpperCase(String[] strings){
        //This returns the entire array that has only the upper case letters.
        int uppcaseCount=0;
        for(i=0;i<strings.length;i++){
        //helper method needed
            if(isUpperCase(strings[i])==true){
               uppcaseCount++;
           }
        }
        String[] res= new String[uppcaseCount];
        int resArrIndex=0;
        for(i=0; i<strings.length;i++){
            if(isUpperCase(strings[i]==true){
                res[resArrIndex]=strings[i];
                resArrIndex++;
            }
        }
        
    }
    public static boolean isUpperCase(String s){
        boolean result=false;
        if(s.match(("[A-Z]")==true){
            result=true;
        }
        return result;
    }
    public static void main (String[] args){
        String[] test = new String[3];
        test[0]="AWERDEADCF";
        test[1]="1247319";
        test[2]="AWSEFDJ123";
        int res=countUpperCase(test);
        System.out.println(res);
    }
}